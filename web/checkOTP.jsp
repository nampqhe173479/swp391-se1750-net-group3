<%-- 
    Document   : checkOTP
    Created on : 20-Jan-2024, 15:33:59
    Author     : ADMIN
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">

    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>Check OTP</title>

        <!-- Custom fonts for this template-->
        <link href="vendor2/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
        <link
            href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
            rel="stylesheet">

        <!-- Custom styles for this template-->
        <link href="css2/sb-admin-2.min.css" rel="stylesheet">

    </head>

    <body style="background-color: whitesmoke">

        <div class="container">

            <!-- Outer Row -->
            <div class="row justify-content-center">

                <div class="col-xl-10 col-lg-12 col-md-9">

                    <div class="card o-hidden border-0 shadow-lg my-5">
                        <div class="card-body p-0">
                            <!-- Nested Row within Card Body -->
                            <div class="row">
                                <div class="col-lg-6">
                                    <img src="./img/_93bdffce-1071-49d2-864e-6ee5694f2895.jpg" alt="Image" class="img-fluid">
                                </div>
                                <div class="col-lg-6">
                                    <div class="p-5">
                                        <div class="text-center">
                                            <h1 class="h4 text-gray-900 mb-2">Check OTP</h1>
                                            <p class="mb-4">This page allows users to check their OTP for verification.</p>
                                        </div>
                                        <form action="CheckOTPController" method="post" class="user">
                                     
                                            <div class="form-group">
                                                <input type="number" class="form-control form-control-user"
                                                       id="exampleInputEmail" aria-describedby="emailHelp"
                                                       name="OTP" placeholder="Enter OTP in your mailbox...">
                                            </div>
                                            <button type="submit" class="btn btn-primary btn-user btn-block" style="background-color: #3B5D50">
                                                Send OTP
                                            </button>
                                        </form>
                                        <br>
                                        <div class="text-center text-black">
                                            <p>${mess}</p>
                                        </div>
                                        <hr>
                                        <div class="text-center">
                                            <a class="small" href="registration.jsp">Create an Account!</a>
                                        </div>
                                        <div class="text-center">
                                            <a class="small" href="login.jsp">Already have an account? Login!</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

            </div>

        </div>

        <!-- Bootstrap core JavaScript-->
        <script src="vendor2/jquery/jquery.min.js"></script>
        <script src="vendor2/bootstrap/js/bootstrap.bundle.min.js"></script>

        <!-- Core plugin JavaScript-->
        <script src="vendo2r/jquery-easing/jquery.easing.min.js"></script>

        <!-- Custom scripts for all pages-->
        <script src="js2/sb-admin-2.min.js"></script>

    </body>
</html>
