/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Brand;
import model.Color;
import model.Product;

/**
 *
 * @author PCASUS
 */
public class ProductDAO extends DBContext {

    public List<Product> SortByPriceASC() {
        List<Product> productList = new ArrayList<>();
        String sql = "SELECT product_id, product_name, imgPath, price, description, quantity, brand_id FROM Products ORDER BY price ASC";

        try ( PreparedStatement st = connection.prepareStatement(sql);  ResultSet rs = st.executeQuery()) {

            while (rs.next()) {
                int productId = rs.getInt("product_id");
                int brandId = rs.getInt("brand_id");
                String productName = rs.getString("product_name");
                int price = rs.getInt("price");
                String imgPath = rs.getString("imgPath");
                String description = rs.getString("description");
                int quantity = rs.getInt("quantity");

                // Assuming you have a constructor in your Product class that accepts all these parameters
                Product product = new Product(productId, brandId, productName, price, imgPath, description, Integer.MIN_VALUE, Integer.MIN_VALUE, quantity, null, Integer.MIN_VALUE);
                productList.add(product);
            }

        } catch (SQLException e) {
            // Handle the exception appropriately, e.g., log or rethrow
            System.err.println("Error retrieving products: " + e.getMessage());
            e.printStackTrace();
        }

        return productList;
    }

    public List<Product> getAllProducts() {
        List<Product> productList = new ArrayList<>();
        String sql = "SELECT product_id, product_name, imgPath, price, description, quantity, brand_id FROM Products";

        try ( PreparedStatement st = connection.prepareStatement(sql);  ResultSet rs = st.executeQuery()) {

            while (rs.next()) {
                int productId = rs.getInt("product_id");
                int brandId = rs.getInt("brand_id");
                String productName = rs.getString("product_name");
                int price = rs.getInt("price");
                String imgPath = rs.getString("imgPath");
                String description = rs.getString("description");
                int quantity = rs.getInt("quantity");

                // Assuming you have a constructor in your Product class that accepts all these parameters
                Product product = new Product(productId, brandId, productName, price, imgPath, description, Integer.MIN_VALUE, Integer.MIN_VALUE, quantity, null, Integer.MIN_VALUE);
                productList.add(product);
            }

        } catch (SQLException e) {
            // Handle the exception appropriately, e.g., log or rethrow
            System.err.println("Error retrieving products: " + e.getMessage());
        }

        return productList;
    }

    

    public boolean insertNewPlant(int cateId, String name, int price, String imgPath, String description, int year, int gear, int quantity, String version, int color_id) {
        boolean check = false;
        try {
            String sql = "INSERT INTO [dbo].[Products]\n"
                    + "           ([Brand_id]\n"
                    + "           ,[product_Name]\n"
                    + "           ,[price]\n"
                    + "           ,[imgPath]\n"
                    + "           ,[description]\n"
                    + "           ,[year]\n"
                    + "           ,[gear]\n"
                    + "           ,[quantity]\n"
                    + "           ,[version]\n"
                    + "           ,[color_id])\n"
                    + "     VALUES\n"
                    + "           (?,\n"
                    + "		   ?,\n"
                    + "		   ?,\n"
                    + "		   ?,\n"
                    + "		   ?,\n"
                    + "		   ?,\n"
                    + "		   ?,\n"
                    + "		   ?,\n"
                    + "		   ?,\n"
                    + "		   ?)";

            PreparedStatement stm = connection.prepareStatement(sql);

            stm.setInt(1, cateId);
            stm.setString(2,name);
            stm.setInt(3, price);
            stm.setString(4, imgPath);
            stm.setString(5, description);
            stm.setInt(6, year);
            stm.setInt(7, gear);
            stm.setInt(8, quantity);
             stm.setString(9, version);
            stm.setInt(10, color_id);
            check = stm.executeUpdate() > 0 ? true : false;
        } catch (Exception ex) {
            Logger.getLogger(ProductDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return check;
    }
    
    
    public List<Product> getAllProduct() {
        List<Product> list = new ArrayList<>();
        String sql = "select * from Products";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                list.add(new Product(rs.getInt(1),rs.getInt(2) , rs.getString(3), rs.getInt(4), rs.getString(5), rs.getString(6), rs.getInt(7), rs.getInt(8), rs.getInt(9), rs.getString(10),rs.getInt(11)));
                
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;

    }
    
    public List<Product> SearchByName(String txtSearch) {
        List<Product> list = new ArrayList<>();
        String sql = "select * from Products where product_Name like ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, "%" + txtSearch + "%");
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                
                list.add(new Product(rs.getInt(1),rs.getInt(2) , rs.getString(3), rs.getInt(4), rs.getString(5), rs.getString(6), rs.getInt(7), rs.getInt(8), rs.getInt(9), rs.getString(10),rs.getInt(11)));
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return list;

    }
    
    public List<Product> getLast() {
        List<Product> list=new ArrayList<>();
        String sql = "select top 5 * from Products order by product_id desc";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                
                list.add(new Product(rs.getInt(1),rs.getInt(2) , rs.getString(3), rs.getInt(4), rs.getString(5), rs.getString(6), rs.getInt(7), rs.getInt(8), rs.getInt(9), rs.getString(10),rs.getInt(11)));
            }
        } catch (SQLException ex) {
           ex.printStackTrace();
        }
        return list;

    }
    
    public Product getProductByID(String id) {
       
        String sql = "select * from Products where product_id=?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, id);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                
                return new Product(rs.getInt(1),rs.getInt(2) , rs.getString(3), rs.getInt(4), rs.getString(5), rs.getString(6), rs.getInt(7), rs.getInt(8), rs.getInt(9), rs.getString(10),rs.getInt(11));

            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return null;
    }
    public List<Product> getProductByBrandID(String id) {
        List<Product> list=new ArrayList<>();
        String sql = "select * from Products where Brand_id=?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, id);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                
            list.add(new Product(rs.getInt(1),rs.getInt(2) , rs.getString(3), rs.getInt(4), rs.getString(5), rs.getString(6), rs.getInt(7), rs.getInt(8), rs.getInt(9), rs.getString(10),rs.getInt(11)));
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }

    public List<Product> getProductByBrandId() {
         List<Product> list = new ArrayList<>();
        String sql = "select * from Products where Brand_id = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                list.add(new Product(rs.getInt(1),rs.getInt(2) , rs.getString(3), rs.getInt(4), rs.getString(5), rs.getString(6), rs.getInt(7), rs.getInt(8), rs.getInt(9), rs.getString(10),rs.getInt(11)));
                
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }

    public List<Product> SortByPriceDESC() {
       List<Product> productList = new ArrayList<>();
        String sql = "SELECT product_id, product_name, imgPath, price, description, quantity, brand_id FROM Products ORDER BY price DESC";

        try ( PreparedStatement st = connection.prepareStatement(sql);  ResultSet rs = st.executeQuery()) {

            while (rs.next()) {
                int productId = rs.getInt("product_id");
                int brandId = rs.getInt("brand_id");
                String productName = rs.getString("product_name");
                int price = rs.getInt("price");
                String imgPath = rs.getString("imgPath");
                String description = rs.getString("description");
                int quantity = rs.getInt("quantity");

                // Assuming you have a constructor in your Product class that accepts all these parameters
                Product product = new Product(productId, brandId, productName, price, imgPath, description, Integer.MIN_VALUE, Integer.MIN_VALUE, quantity, null, Integer.MIN_VALUE);
                productList.add(product);
            }

        } catch (SQLException e) {
            // Handle the exception appropriately, e.g., log or rethrow
            System.err.println("Error retrieving products: " + e.getMessage());
        }

        return productList;
    }
    public static void main(String[] args) {
        ProductDAO a =new ProductDAO();
         List<Product> b = a.SortByPriceASC();
        for (Product product : b) {
            System.out.println(product.getPrice());
        }
    }

}
