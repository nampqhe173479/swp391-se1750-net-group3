/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Brand;
import model.Color;

/**
 *
 * @author PCASUS
 */
public class BrandDAO extends DBContext {

    public Map<Integer, String> getCategories() {
        Map<Integer, String> list = new LinkedHashMap<>();
        String sql = "SELECT brand_id, brand_name FROM Brands";

        try ( PreparedStatement st = connection.prepareStatement(sql);  ResultSet rs = st.executeQuery()) {

            if (rs != null) {
                while (rs.next()) {
                    int cateId = rs.getInt("brand_id");
                    String cateName = rs.getString("brand_name");
                    list.put(cateId, cateName);
                }
            }

        } catch (SQLException e) {
            // Handle the exception appropriately, e.g., log or rethrow
            System.err.println("Error retrieving products: " + e.getMessage());
        }

        return list;
    }
//
//    public static void main(String[] args) {
//        BrandDAO brandDAO = new BrandDAO();
//
//        // Test the getCategories method
//        Map<Integer, String> brandMap = brandDAO.getCategories();
//
//        // Display the retrieved brands
//        for (Map.Entry<Integer, String> entry : brandMap.entrySet()) {
//            System.out.println("Brand ID: " + entry.getKey() + ", Brand Name: " + entry.getValue());
//        }
//    }

    public ArrayList<Brand> getAllBrands() {
        ArrayList<Brand> certs = new ArrayList<>();
        try {

            String sql = "SELECT brand_id, brand_name FROM Brands";
            PreparedStatement stm = connection.prepareStatement(sql);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                Brand a = new Brand(rs.getInt(1), rs.getString(2));
                certs.add(a);
            }
        } catch (SQLException ex) {
            Logger.getLogger(BrandDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return certs;
    }
    
    public static void main(String[] args) {
        BrandDAO BrandDAO = new BrandDAO();

        // Test the getAllProducts method
        ArrayList<Brand> productList = BrandDAO.getAllBrands();

        // Display the retrieved products
        for (Brand brand : productList) {
            System.out.println(brand.getBrand_Name());
        }
    }

    

}
