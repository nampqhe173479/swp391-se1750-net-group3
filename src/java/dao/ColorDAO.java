/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Color;

/**
 *
 * @author PCASUS
 */
public class ColorDAO extends DBContext {

    public ArrayList<Color> getAllColors() {
        ArrayList<Color> certs = new ArrayList<>();
        try {

            String sql = "SELECT color_id, color_name FROM Colors";
            PreparedStatement stm = connection.prepareStatement(sql);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                Color a = new Color(rs.getInt(1), rs.getString(2));
                certs.add(a);
            }
        } catch (SQLException ex) {
            Logger.getLogger(ColorDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return certs;
    }
    
    public static void main(String[] args) {
        ColorDAO ColorDAO = new ColorDAO();

        // Test the getAllProducts method
        ArrayList<Color> productList = ColorDAO.getAllColors();

        // Display the retrieved products
        for (Color color : productList) {
            System.out.println(color.getColor_name());
        }
    }
    
}
