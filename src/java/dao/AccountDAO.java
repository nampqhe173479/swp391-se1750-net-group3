/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import model.Account;

/**
 *
 * @author PCASUS
 */
public class AccountDAO extends DBContext {

    // CRUD -> input Object ouput: Object update delete-> true false (throw ex), method getID getCusstomerID, getProduct(ID) product, brand, colors
    // brand, color | getBrandID -> Brand | getColorID -> color 
    // new p
    //  p.brand = BrandAO.getBrandID()
    public Account getAccount(String email, String password) {
        Account acc = null;
        try {
            String sql = "SELECT account_id, email, password, fullName, phone, status, role FROM Accounts WHERE email = ? AND password = ?";
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, email);
            stm.setString(2, password);

            ResultSet rs = stm.executeQuery();
            if (rs.next()) {
                int AccId = rs.getInt("account_id");
                String Email = rs.getString("email");
                String FullName = rs.getString("fullName");
                String Phone = rs.getString("phone");
                int Status = rs.getInt("status");
                int Role = rs.getInt("role");
                acc = new Account(AccId, Email, "******", FullName, Status, Phone, Role);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return acc;
    }

    public boolean updateToken(String token, String email) {
        boolean check = false;
        try {
            String sql = "UPDATE Accounts Set token = ? WHERE email = ?";
            try ( PreparedStatement psm = connection.prepareStatement(sql)) {
                psm.setString(1, token);
                psm.setString(2, email);
                check = psm.executeUpdate() > 0;
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return check;
    }

    public Account getAccountInfoByEmail(String email) {
        Account acc = null;
        try {
            String sql = "SELECT account_id, email, password, FullName, Phone, Status, Role FROM Accounts WHERE Email = ?";
            try ( PreparedStatement stm = connection.prepareStatement(sql)) {
                stm.setString(1, email);
                try ( ResultSet rs = stm.executeQuery()) {
                    if (rs.next()) {
                        int AccId = rs.getInt("account_id");
                        String Email = rs.getString("Email");
                        String Password = rs.getString("Password");
                        String FullName = rs.getString("FullName");
                        String Phone = rs.getString("Phone");
                        int Status = rs.getInt("Status");
                        int Role = rs.getInt("Role");
                        acc = new Account(AccId, Email, Password, FullName, Status, Phone, Role);
                    }
                }
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return acc;
    }

    public boolean insertAccount(String newEmail, String newPassword, String newFullname, String newPhone, int newStatus, int newRole) {
        boolean check = false;
        String sql = "INSERT INTO Accounts (email, password, fullname, phone, status, role) VALUES (?, ?, ?, ?, ?, ?)";
        try {
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, newEmail);
            stm.setString(2, newPassword);
            stm.setString(3, newFullname);
            stm.setString(4, newPhone);
            stm.setInt(5, newStatus);
            stm.setInt(6, newRole);
            check = stm.executeUpdate() > 0 ? true : false;
        } catch (Exception e) {
            System.out.println(e);
        }
        return check;
    }

    public List<Account> getAccounts() {
        List<Account> list = new ArrayList<>();
        try {
            String sql = "SELECT account_id, Email, Password, FullName, Phone, Status, Role FROM Accounts";
            PreparedStatement stm = connection.prepareStatement(sql);
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                int AccId = rs.getInt("account_id");
                String Email = rs.getString("Email");
                String Password = rs.getString("Password");
                String FullName = rs.getString("FullName");
                String Phone = rs.getString("Phone");
                int Status = rs.getInt("Status");
                int Role = rs.getInt("Role");
                Account acc = Account.builder()
                        .accId(AccId)
                        .email(Email)
                        .password(Password)
                        .fullName(FullName)
                        .status(Status)
                        .phone(Phone)
                        .role(Role)
                        .build();
                list.add(acc);
            }

        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return list;
    }

    // Inside your AccountDAO class
    public boolean updateAccount(Account account) {
        boolean check = false;
        try {
            String sql = "UPDATE Accounts SET password = ?, fullName = ?, phone = ?, status = ?, role = ? WHERE account_id = ?";
            try ( PreparedStatement stm = connection.prepareStatement(sql)) {
                stm.setString(1, account.getPassword());
                stm.setString(2, account.getFullName());
                stm.setString(3, account.getPhone());
                stm.setInt(4, account.getStatus());
                stm.setInt(5, account.getRole());
                stm.setInt(6, account.getAccId());

                check = stm.executeUpdate() > 0;
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return check;
    }

    public Account getAccountByEmail(String email) {
        Account acc = null;
        try {
            String sql = "SELECT account_id, email, password, fullName, phone, status, role FROM Accounts WHERE email = ?";
            try ( PreparedStatement stm = connection.prepareStatement(sql)) {
                stm.setString(1, email);

                ResultSet rs = stm.executeQuery();
                if (rs.next()) {
                    int accId = rs.getInt("account_id");
                    String Email = rs.getString("email");
                    String Password = rs.getString("password");
                    String FullName = rs.getString("fullName");
                    String Phone = rs.getString("phone");
                    int Status = rs.getInt("status");
                    int Role = rs.getInt("role");
                    acc = new Account(accId, Email, Password, FullName, Status, Phone, Role);
                }
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return acc;
    }

    public boolean addAccount(Account newAccount) {
        boolean check = false;
        String sql = "INSERT INTO Accounts (email, password, fullname, phone, status, role) VALUES (?, ?, ?, ?, ?, ?)";
        try {
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, newAccount.getEmail());
            stm.setString(2, newAccount.getPassword());
            stm.setString(3, newAccount.getFullName());
            stm.setString(4, newAccount.getPhone());
            stm.setInt(5, newAccount.getStatus());
            stm.setInt(6, newAccount.getRole());

            check = stm.executeUpdate() > 0;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return check;
    }

    public boolean changeAccount(String email, String newFullname, String newPhone) throws SQLException {
        boolean check = false;
        String sql = "UPDATE Accounts SET fullname = ?, phone = ? WHERE email = ?";
        try {
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, newFullname);
            stm.setString(2, newPhone);
            stm.setString(3, email);

            check = stm.executeUpdate() > 0;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return check;
    }

    public boolean checkOldPassword(int accId, String oldPassword) throws SQLException {
        boolean check = false;
        ResultSet rs = null;
        String sql = "SELECT Password FROM Accounts WHERE account_id = ?";
        try {
            PreparedStatement stm = connection.prepareStatement(sql);

            stm.setInt(1, accId);
            rs = stm.executeQuery();
            if (rs.next()) {
                String accPsw = rs.getString("Password");
                if (accPsw.equalsIgnoreCase(oldPassword)) {
                    check = true;
                }
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return check;
    }

    public boolean updateAccountPassword(int accId, String newPassword) throws SQLException {
        boolean check = false;
        String sql = "UPDATE Accounts Set password = ? WHERE account_id = ?";
        try {
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, newPassword);
            stm.setInt(2, accId);

            check = stm.executeUpdate() > 0;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return check;

    }
    
    public boolean updateAccountPassword(String newPassword, int accId) throws SQLException {
        boolean check = false;
        String sql = "UPDATE Accounts Set password = ? WHERE account_id = ?";
        try {
            PreparedStatement stm = connection.prepareStatement(sql);
            stm.setString(1, newPassword);
            stm.setInt(2, accId);

            check = stm.executeUpdate() > 0;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return check;

    }

}
