/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */

package controller.sync;

import dao.AccountDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import model.Account;
import model.Email;

/**
 *
 * @author ADMIN
 */
@WebServlet(name="ForgotPassController", urlPatterns={"/ForgotPassController"})
public class ForgotPassController extends HttpServlet {
   
    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ForgotPassController</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet ForgotPassController at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    } 

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    
    private boolean isValidEmail(String email) {
        String regex = "^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\\.[A-Z|a-z]{2,}$";
        return email.matches(regex);
    }
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    } 

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        String email = request.getParameter("email");
      
        AccountDAO accountDAO = new AccountDAO();
        Account account = accountDAO.getAccountByEmail(email);
        
        if (account != null) {
            Email emailSender = new Email();
            String OTP = emailSender.generateRandomOTP(6);
            long timeInMillis = 2 * 60 * 1000; 
            long currentTime = System.currentTimeMillis();
            long timeStorage = currentTime + timeInMillis;
            
            HttpSession session = request.getSession();
            session.setAttribute("generatedOTP", OTP);
            session.setAttribute("timeStorage", timeStorage);
            session.setAttribute("accountID", account.getAccId());
            
            boolean emailSent = emailSender.sendEmail(email,
                    "[Show Room] YOUR PASSWORD",
                    "<html><body>"
                    + "<p>Dear " + account.getFullName() + ",</p>"
                    + "<p>Your OTP is: <strong>" + OTP + "</strong></p>"
                    + "<p>Thank you for using our services.</p>"
                    + "<p>Best regards,<br/>FPT DriveSign.</p>"
                    + "</body></html>");
            if (emailSent) {
                request.getRequestDispatcher("checkOTP.jsp").forward(request, response);
                return;
            } else {
                request.setAttribute("mess", "Failed to send email!");
            }
        } else {
            request.setAttribute("mess", "Invalid email!");
        }

        request.getRequestDispatcher("forgotPass.jsp").forward(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
