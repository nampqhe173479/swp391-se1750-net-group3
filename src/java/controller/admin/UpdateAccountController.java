/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */

package controller.admin;

import dao.AccountDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import model.Account;

/**
 *
 * @author PCASUS
 */
@WebServlet(name="UpdateAccountController", urlPatterns={"/UpdateAccountController"})
public class UpdateAccountController extends HttpServlet {
   
    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
         response.setContentType("text/html;charset=UTF-8");
        try {
            String action = request.getParameter("action");

            if ("blockAccount".equals(action)) {
                String email = request.getParameter("email");
                // Perform blocking logic here
                // For example, you might call a method in your DAO to update the account status
                AccountDAO dao = new AccountDAO();
                Account account = dao.getAccountInfoByEmail(email);
                if (account != null) {
                    account.setStatus(0); // Assuming 0 means blocked
                    dao.updateAccount(account);
                    request.setAttribute("MSG_SUCCESS", "Account blocked successfully.");
                } else {
                    request.setAttribute("MSG_ERROR", "Account not found.");
                }
            } else if ("unblockAccount".equals(action)) {
                String email = request.getParameter("email");
                // Perform unblocking logic here
                // For example, you might call a method in your DAO to update the account status
                AccountDAO dao = new AccountDAO();
                Account account = dao.getAccountInfoByEmail(email);
                if (account != null) {
                    account.setStatus(1); // Assuming 1 means active
                    dao.updateAccount(account);
                    request.setAttribute("MSG_SUCCESS", "Account unblocked successfully.");
                } else {
                    request.setAttribute("MSG_ERROR", "Account not found.");
                }
            }

            // Redirect back to the account management page
            response.sendRedirect("AdminManageAccountController");

        } catch (Exception e) {
            log("Error at UpdateAccountController: " + e.toString());
        }
    } 

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    } 

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
