/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

/**
 *
 * @author PCASUS
 */
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.Getter;
import lombok.ToString;

@Builder
@NoArgsConstructor
//@AllArgsConstructor
@Getter
@Setter
@ToString

public class Product {

    private int product_id;
    private int Brand_id;
    private String product_Name;
    private int price;
    private String imgPath;
    private String description;
    private int year;
    private int gear;
    private int quantity;
    private String version;
    private int color_id;
//

    public Product(int product_id, int Brand_id, String product_Name, int price, String imgPath, String description, int year, int gear, int quantity, String version, int color_id) {
        this.product_id = product_id;
        this.Brand_id = Brand_id;
        this.product_Name = product_Name;
        this.price = price;
        this.imgPath = imgPath;
        this.description = description;
        this.year = year;
        this.gear = gear;
        this.quantity = quantity;
        this.version = version;
        this.color_id = color_id;
    }
   
    
    

}
