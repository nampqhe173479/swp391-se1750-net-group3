/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 *
 * @author PCASUS
 */
//
//@Builder
//@NoArgsConstructor
//@AllArgsConstructor
//@Getter
//@Setter
//@ToString
public class Brand {
    private  int brand_id;
    private  String brand_Name;

    public Brand(int brand_id, String brand_Name) {
        this.brand_id = brand_id;
        this.brand_Name = brand_Name;
    }

    public Brand() {
    }
    
    

    public int getBrand_id() {
        return brand_id;
    }

    public String getBrand_Name() {
        return brand_Name;
    }


    
    
    
}
